const puppeteer = require('puppeteer');

const parse = async (article_number) => {
    const browser = await puppeteer.launch({headless: false});
    const page = await browser.newPage();
    await page.goto(`https://www.wildberries.ru/catalog/${article_number}/detail.aspx?targetUrl=MI`);
    await page.waitFor(3000);
    try {
        const result = await page.evaluate((article_number) => {
            let code = article_number
                let base_price = document.querySelector('.price-block__final-price').innerText;
                let title = document.querySelectorAll('.same-part-kt__header')[0].innerText;
                base_price = base_price.slice(0,-2)
                base_price = Number(base_price.replace(/\s+/g, ''))
                return {
                    code,
                    title,
                    base_price
            } 
        });
        browser.close();
        return result;
    } catch (e) {
        console.log('>>>>>>>>>>>>>>>>>', e)
        browser.close();
    };
}
module.exports = {parse}

require('dotenv').config()
const express = require('express')
const app = express()
const Bot = require('./bot.js')

const bot = new Bot();

const runServer = async() => {
  try {
    await app.listen(process.env.SERVER_PORT, () => {
      console.log('server run')
    })
    bot.createBot(process.env.BOT_TOKEN)
  } catch (e) {
    console.log(e)
  }
}

runServer()



const { Client } = require('pg')
require('dotenv').config()

class SqlController {
  constructor () {
    this.client = new Client({
      host: process.env.POSTGRES_HOST,
      user: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_DB
    })
  }
  async connection () {
    await this.client.connect()
    console.log('Connect sucsessful')
  }

  sqlRequest (sql) {
    return  new Promise((resolve, reject) => {
      this.client.query(sql, (err, result) => {
        if (err) {
          console.log('ERROR')
          reject(err)
        } else {
          resolve(result)
        }
      })
    })
  }
  async sqlIsExist (identificator, table) {
    return new Promise((resolve, reject) => {
      let sql = `select id from ${table} where id=${identificator};`
      this.client.query(sql,(err, result) => {
        if (!err) {
          console.log('Request "isExist?" sucsessful')
          resolve(result)
        } else {
          console.log(err)
          reject(err)
        }
      })
    })
  }

}

module.exports = {
  SqlController
}
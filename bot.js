const { Telegraf } = require('telegraf')
const { SqlController } = require('./sql')
const { BotController } = require('./botControl')
const { parse } = require('./parseCite')
const { update } = require('./updater')

class Bot {
  constructor() {
  }
  async createBot(token) {
    const bot = new Telegraf(process.env.BOT_TOKEN)

    async function check () {
      const activeOrders = await update()
      let objSql = new SqlController()
      await objSql.connection()
      for (let i=0; i<activeOrders.length; i++) {
        let articl_id  = activeOrders[i].product_id 
        let sqlProductInfo = `SELECT * FROM products WHERE id=${articl_id};`

        let baseProduct = (await objSql.sqlRequest(sqlProductInfo))
        let basePrice = baseProduct.rows[0].base_price

        let currentProduct = await parse(articl_id)
        let currentPrice = currentProduct.base_price
          console.log(basePrice, currentPrice, articl_id, baseProduct.rows[0].title)
        if (currentPrice != basePrice && currentPrice != baseProduct.rows[0].current_price) {
          let sqlUpdatePrice = `UPDATE products SET current_price = ${currentPrice} where id=${articl_id};`
          await objSql.sqlRequest(sqlUpdatePrice)
            console.log('WAS CHANGED', basePrice, currentPrice, articl_id, baseProduct.rows[0].title)
          await bot.telegram.sendMessage(activeOrders[i].chat_id, `Название: ${baseProduct.rows[0].title}\nЦена базовая: ${basePrice} руб\nЦена текущая: ${currentPrice} руб\nСсылка: https://www.wildberries.ru/catalog/${articl_id}/detail.aspx?targetUrl=MI`)
        }
          continue
      }
      console.log()
    }

    check()
    setInterval(() => {
      check()}, 1800000)

    bot.start(async (ctx) => {
      let objSql = new SqlController()
      let controller = new BotController()
      controller.startBot(ctx, objSql)
    })

    bot.help((ctx) => ctx.reply('Ввудите /start для регистрации.\nВведите запрос: "следи <артикул продукта>.\nНапример: "следи 59918929" (без ковычек, пробел только в середине).\nБот будет следить за изменением цены относительно начальной(базовой) стоимости и информировать о нем')) 

    bot.on('message', async(ctx) => {
      let messageText = ctx.message.text.toLowerCase()
      if (messageText.slice(0, 5) == 'следи')  {
        let objSql = new SqlController()
        let article_number = messageText.slice(6, )
        let prodInfo = await parse(article_number)
        console.log('START TO FOLLOW',prodInfo)
        await objSql.connection()
        let sqlExistsProduct = `select id from products where id=${article_number};`
        let sqlExistsOrder = `select id from orders where chat_id=${ctx.from.id} and product_id=${article_number} ;`
        try {
          const existsProduct = await objSql.sqlRequest(sqlExistsProduct)
          const existsOrder = await objSql.sqlRequest(sqlExistsOrder)
          if (existsProduct.rows[0] == undefined) {
            let sqlAddProduct = `insert into products(id, title, base_price, current_price) values(${article_number}, '${prodInfo.title}', ${prodInfo.base_price}, ${prodInfo.base_price});`
            try {
              await objSql.sqlRequest(sqlAddProduct)
            } catch (e) {
              console.log(`Sql create  error`, e)
            }
            ctx.reply('товар успешно занесен в БД')
          } 
          if (existsOrder.rows[0] == undefined) {
            let sqlAddOrder = `insert into orders (chat_id, product_id) values (${ctx.from.id}, ${article_number})`
            try {
              await objSql.sqlRequest(sqlAddOrder)
            } catch (e) {
            console.log('Error: ', e)
            }
          } 
          else {
            ctx.reply(`товар УЖЕ занесен в БД`)
          }
        } catch (e) {
          console.log('Error: ',e)
        }
        ctx.reply(`Название: ${prodInfo.title}\nЦена базовая: ${prodInfo.base_price} руб`)
      }
      // if (messageText.slice(0, 6) == 'обнови') {

      // }
      else { 
        ctx.reply('Нет такой команды')
      }
    })

    bot.launch() // запуск бота
  }
}

module.exports = Bot





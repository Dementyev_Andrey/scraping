const { Client } = require('pg')
require('dotenv').config()

const client = new Client({
    host: process.env.POSTGRES_HOST,
    user: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DB
  })

  client.connect((error, client) => {
        if (error) {
          console.log(error)
        } else {
          console.log(`Connected to database`)
        }
      })
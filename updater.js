const {SqlController} = require('./sql')
const { parse } = require('./parseCite')

const update = async() => {
    const sqlActiveOrders = `SELECT * FROM orders WHERE is_active=true;`;
    const objSql = new SqlController();
    await objSql.connection();

    const activeOrders = await objSql.sqlRequest(sqlActiveOrders)
    return activeOrders.rows
}

module.exports = {update}
